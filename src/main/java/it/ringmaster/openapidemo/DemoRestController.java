package it.ringmaster.openapidemo;

import java.util.Collection;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import it.ringmaster.openapidemo.datamodel.Demo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@OpenAPIDefinition(
        info = @Info(
                title = "OpenAPI Demo",
                version = "1.0",
                description = "OpenAPI Demo description"
        )
)
@RequestMapping("/api/demo")
public interface DemoRestController {
    @GetMapping("/{id}")
    @Operation(summary = "Returns the demo object with the given id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return demo with the given id"),
                    @ApiResponse(responseCode = "500", description = "Unexpected server error"),
            }
    )
    Demo findById(@PathVariable String id);

    @GetMapping("/")
    @Operation(summary = "Returns 10 demo objects",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return 10 demo objects",
                            content = @Content(array = @ArraySchema(schema = @Schema(implementation = Demo.class)))),
                    @ApiResponse(responseCode = "500", description = "Unexpected server error"),
            }
    )
    Collection<Demo> findAll();

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete the demo object with the given id",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Deleted successfully"),
                    @ApiResponse(responseCode = "500", description = "Unexpected server error"),
            }
    )
    void deleteById(@PathVariable String id);
}
