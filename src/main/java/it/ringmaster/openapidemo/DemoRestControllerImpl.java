package it.ringmaster.openapidemo;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

import it.ringmaster.openapidemo.datamodel.Demo;
import it.ringmaster.openapidemo.datamodel.Detail;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoRestControllerImpl implements DemoRestController {

    @Override
    public Demo findById(String id) {
        return generateDemoById(id);
    }

    @Override
    public Collection<Demo> findAll() {
        return Stream.generate(() -> generateDemoById(UUID.randomUUID().toString()))
                .limit(10)
                .collect(toList());
    }

    @Override
    public void deleteById(String id) {
        // un bel nop
    }

    private static Demo generateDemoById(String id) {
        return new Demo(id, new Detail("detail #" + id));
    }
}
