package it.ringmaster.openapidemo.datamodel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "The details of the demo value object")
public class Detail {
    @Schema(description = "The detail string for the detail value object")
    private final String detail;
}
