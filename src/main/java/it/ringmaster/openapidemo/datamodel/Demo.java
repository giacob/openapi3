package it.ringmaster.openapidemo.datamodel;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "Demo value object")
public class Demo {

    @Schema(description = "The id of the demo value object")
    private final String id;
    private final Detail detail;
}
